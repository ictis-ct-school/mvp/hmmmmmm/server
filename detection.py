# Other imports
from ultralytics import YOLO
import cv2

# Inner imports
from common.pereferences import MODEL_PATH

class Detector:
    def __init__(self, path_to_weights = MODEL_PATH):
        self.model = YOLO(path_to_weights)

    def __str__(self):
        self.model.info(verbose=True)
        return f'|||Succsess|||'

    def detect(self, image):
        if type(image) is str:
            image = cv2.imread(image)

        res = self.model(image[:,:,::-1])
        boxes = res[0].boxes.xyxy

        # print(f'Detection result: {res}')
        
        for box in boxes:
            box = [int(cord) for cord in box]
            cv2.rectangle(image, (box[0], box[1]), (box[2], box[3]), (222, 222, 0),  2)

        return image

  
if __name__ == "__main__":
    model = Detector()

    result = model.detect('data/test_send.jpg')
    cv2.imshow('res', result)
    cv2.waitKey(0)