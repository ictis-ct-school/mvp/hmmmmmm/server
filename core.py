from flask import Flask

app = Flask(__name__)

import routes

app.run(debug=True, host="0.0.0.0")
