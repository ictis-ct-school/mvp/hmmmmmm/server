from PIL import Image
import numpy as np
from flask import request, jsonify
from base64 import encodebytes
import io
import cv2
from __main__ import app


def pilllow_to_opencv(pillow_input):
    open_cv_image = np.array(pillow_input)
    open_cv_image = open_cv_image[:, :, ::-1].copy()

    return open_cv_image

def opencv_to_pillow(opencv_input):
    pillow_image = cv2.cvtColor(opencv_input, cv2.COLOR_GRAY2RGB)
    pillow_image = Image.fromarray(pillow_image)

    return pillow_image


@app.route("/")
def hello():
    return "Hello, people!"


@app.route("/ref_image", methods=["POST"])
def ref_image():
    file = request.files['raw_img']
    frame = Image.open(file.stream)

    img = pilllow_to_opencv(frame)

    grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    for_resp = opencv_to_pillow(grayscale)

    byte_arr = io.BytesIO()
    for_resp.save(byte_arr, format="PNG")
    encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii')

    return jsonify({'img': encoded_img, 'data': 'test resp'})

